package com.jasperreport.controller;

import com.jasperreport.entity.Student;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PdfController {

    @GetMapping("/all-students")
    public ResponseEntity<?> getAllStudent() {
        return ResponseEntity.ok(this.getAllStudentDetails());
    }

    @GetMapping("/pdf/all-students")
    public ResponseEntity<?> getAllStudentPdf() throws FileNotFoundException, JRException {
        String filePath = ResourceUtils.getFile("classpath:pdf/all-students.jrxml").getAbsolutePath();

        List<Student> list = this.getAllStudentDetails();
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "Tajbir");
        parameters.put("allStudentTableData", dataSource);

        JasperReport report = JasperCompileManager.compileReport(filePath);

        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());

        byte[] pdfArray = JasperExportManager.exportReportToPdf(print);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("filename", "all-student.pdf");

        return new ResponseEntity<>(pdfArray, headers, HttpStatus.OK);
    }

    /*-----------------------HELPER METHOD------------------------------*/
    private List<Student> getAllStudentDetails() {
        List<Student> studentList = Arrays.asList(
                new Student(1L, "Tajbir", "01850179564", "tajbirnuva@gmail.com"),
                new Student(2L, "Nuva", "01810014841", "nuva@gmail.com"),
                new Student(3L, "Parizad", "01670305256", "pari@gmail.com"),
                new Student(4L, "Hasnat", "01626737251", "hasnat@gmail.com"),
                new Student(5L, "Ishmam", "01521327998", "ishmam@gmail.com"),
                new Student(6L, "Maruf", "01912883417", "marufmedia50@gmail.com"),
                new Student(7L, "Faisal", "01670305256", "faisal@gmail.com"),
                new Student(8L, "Tawsif", "01825125833", "tawsif@gmail.com"),
                new Student(9L, "Farhan", "01816572474", "farhan.nadim@gmail.com"),
                new Student(10L, "Adiba", "0171103689", "adiba@gmail.com")
        );
        return studentList;
    }
}